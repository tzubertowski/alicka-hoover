# Technical Exercise

# Description
This is a simple simulation of an imaginary robotic hoover navigating with given directions around an imaginary room with established dimentions.

# Table of contents
    * Installation/Setup
    * Usage
    * Technologies
    * Creator

# Installation/Setup:
- You need to install nodejs 10
- then run
    ```
    npm install
    ```
- for tests run
    ```
    npm test
    ```
- for the application run
    ```
    npm start 
    ```
# Usage 

## This program

# Technolgies used:
- jest

## Creator

| Name                |
| ------------------- |
| **Alicja Kaźmierczak** |

# End notes
Although it wasn't required, I decided to add a few lines that check whether the hoover's initial position is whithin the established room. The simulation starts only if it is. 
I did this to protect the code from working on invalid input.